# Bravoure - Block Login Form

## Directive: Code to be added:

        <block-signup-form></block-signup-form>

## Use

It loads the sign up form.

## Dependencies

It uses the angular-run-membership component to do the actual Sign up functionality


### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-block-signup-form": "^1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
