(function () {
    'use strict';

    function blockLoginForm (PATH_CONFIG, $rootScope, $translate, $location, API_Service, $timeout) {
        return {
            restrict: "E",
            replace: "true",
            link: function (scope, element, attrs) {

                element.on('active focus', '.form-control', function (e) {
                    e.stopPropagation();
                    $(this).parent().addClass('focused', (this.value.length > 0));
                });

                element.on('blur change', '.form-control', function (e) {
                    e.stopPropagation();

                    $(this).parent().toggleClass('focused', (this.value.length > 0));

                    var focused = $('.focused');

                    focused.each(function (index, obj) {
                        if ($(this).find('select').val() == '?') {
                            $(this).removeClass('focused');
                        }
                    });
                }).trigger('blur');

            },


            controller: function ($scope, $controller) {

                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

                $scope.form_name = 'login_form';

                $timeout(function () {

                $scope.showForgotPassword = data.includeForgotPasswordLink;
                }, 1000);

                $scope.showIframe = ($scope.$state.params.identifier == data.loginIdentifier) ? true : false;

                $rootScope.$on('hideIframe', function () {
                    $scope.$apply(function () {
                        $scope.showIframe = false;
                    });
                });

                $rootScope.$on('showIframe', function () {
                    $scope.$apply(function () {
                        $scope.showIframe = true;
                    });
                });

                $scope.model = {
                    "client_id": data.dev_client_id,
                    "client_secret": data.dev_client_secret,
                    "grant_type": data.grant_type
                };

                $scope.schema = {
                    "type": "object",
                    "title": "Comment",
                    "properties": {
                        "client_id": {
                            "title": "client_id",
                            "type": "string"
                        },
                        "client_secret": {
                            "title": "client_secret",
                            "type": "string"
                        },
                        "grant_type": {
                            "title": "grant_type",
                            "type": "string"
                        },
                        "username":  {
                            "title": $translate.instant('form.email'),
                            "type": "string",
                            "pattern": "^\\S+@\\S+$",
                            "validationMessage": $translate.instant('form.error.email')
                        },
                        "password":  {
                            "title": $translate.instant('form.password'),
                            "type": "string",
                            "minLength": 4,
                            "validationMessage": $translate.instant('form.error.password')
                        }
                    },
                    "required": ["username","password"]
                };

                $scope.form = [
                    {
                        "key": "client_id",
                        "type": "hidden"
                    },
                    {
                        "key": "client_secret",
                        "type": "hidden"
                    },
                    {
                        "key": "grant_type",
                        "type": "hidden"
                    },
                    {
                        "key": "username",
                        "required": true
                    },
                    {
                        "key": "password",
                        "type": "password",
                        "required": true
                    },
                ];

                if (typeof $scope.module != 'undefined') {

                    $scope.showError = false;
                    $scope.thankyou_show = false;

                }

                $scope.post_url = host + 'oauth/token';

                $scope.link = $location.path();

                $scope.triggerSubmit = function() {
                    $('.block-newsletter-form__form').trigger('submit');
                };

                $scope.onSubmit = function (form) {

                    form.preventDefault();

                    // First we broadcast an event so all fields validate themselves
                    $scope.$broadcast('schemaFormValidate');

                    // Then we check if the form is valid
                    if ($scope[$scope.form_name].$valid) {

                        var formData = {};

                        angular.forEach($scope.schema.properties, function (value, keys) {
                            formData[keys] = $scope[$scope.form_name][keys].$viewValue;
                        });

                        API_Service($scope.post_url, {}).save_no_array({}, formData,
                            function success(data) {

                                $scope.showError = false;
                                $scope.form_success = true;
                                $scope.thankyou_show = true;

                                $scope.model = {
                                    "client_id": data.dev_client_id,
                                    "client_secret": data.dev_client_secret,
                                    "grant_type": data.grant_type
                                };

                                $rootScope.$emit ('loggedIn', data);

                            },

                            function error(data) {
                                $scope.showError = true;

                                $scope.error = data.data.errors[0];
                            });
                    }

                };

            },
            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-block-login-form/block-login-form.html'
        }
    }

    blockLoginForm.$inject = ['PATH_CONFIG', '$rootScope', '$translate', '$location', 'API_Service', '$timeout'];

    angular
        .module('bravoureAngularApp')
        .directive('blockLoginForm', blockLoginForm);
})();
